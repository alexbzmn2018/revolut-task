How to run:

- sh run.sh

OR

- mvn clean install
- java -jar target/revolut-money-transfer-app-1.0-SNAPSHOT.jar server config.yml

Logs folder: ./logs

Stack:

* Dropwizard, H2