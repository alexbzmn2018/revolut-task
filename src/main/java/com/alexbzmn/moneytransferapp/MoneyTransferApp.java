package com.alexbzmn.moneytransferapp;

import com.alexbzmn.moneytransferapp.dao.AccountDAO;
import com.alexbzmn.moneytransferapp.dao.TransferDAO;
import com.alexbzmn.moneytransferapp.resource.AccountResource;
import com.alexbzmn.moneytransferapp.resource.TransferResource;
import com.alexbzmn.moneytransferapp.service.TransferService;
import io.dropwizard.Application;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.jdbi.v3.core.Jdbi;

public class MoneyTransferApp extends Application<MoneyTransferAppConfiguration> {

    public static void main(final String[] args) throws Exception {
        new MoneyTransferApp().run(args);
    }

    @Override
    public String getName() {
        return "revolut-money-transfer-app";
    }

    @Override
    public void initialize(final Bootstrap<MoneyTransferAppConfiguration> bootstrap) {
        bootstrap.addBundle(new MigrationsBundle<MoneyTransferAppConfiguration>() {
            @Override
            public PooledDataSourceFactory getDataSourceFactory(MoneyTransferAppConfiguration moneyTransferAppConfiguration) {
                return moneyTransferAppConfiguration.getDatabase();
            }
        });
    }

    @Override
    public void run(final MoneyTransferAppConfiguration configuration,
                    final Environment environment) {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, configuration.getDatabase(), "h2");

        environment.jersey().register(new AccountResource(jdbi.onDemand(AccountDAO.class)));
        environment.jersey().register(new TransferResource(new TransferService(jdbi.onDemand(TransferDAO.class), jdbi)));
        environment.lifecycle().manage(new RunLiquibaseOnBoot(jdbi.open(), "migrations.xml"));
    }

}
