package com.alexbzmn.moneytransferapp;

import io.dropwizard.lifecycle.Managed;
import liquibase.Liquibase;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.AllArgsConstructor;
import org.jdbi.v3.core.Handle;

@AllArgsConstructor
public class RunLiquibaseOnBoot implements Managed {
    private Handle handle;
    private String liquibaseConfigPath;


    @Override
    public void start() throws Exception {
        Liquibase liquibase = new Liquibase(liquibaseConfigPath, new ClassLoaderResourceAccessor(), new JdbcConnection(handle.getConnection()));
        liquibase.update("");
    }

    @Override
    public void stop() {
        //No need to close the handle
    }
}
