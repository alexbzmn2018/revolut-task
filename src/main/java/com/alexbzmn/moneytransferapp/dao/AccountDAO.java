package com.alexbzmn.moneytransferapp.dao;

import com.alexbzmn.moneytransferapp.model.Account;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@RegisterRowMapper(AccountDAO.AccountRowMapper.class)
public interface AccountDAO {
    @SqlUpdate("insert into account (balance) values (:balance)")
    @GetGeneratedKeys
    int insert(@BindBean Account account);

    @SqlQuery("select * from account")
    List<Account> fetchAll();

    @SqlQuery("select * from account where id = :id")
    Account findById(@Bind("id") int id);

    @SqlUpdate("update account set balance = :balance where id = :id")
    void update(@BindBean Account account);

    class AccountRowMapper implements RowMapper<Account> {
        @Override
        public Account map(ResultSet rs, StatementContext ctx) throws SQLException {
            return Account.builder()
                    .balance(rs.getBigDecimal("balance"))
                    .id(rs.getInt("id"))
                    .build();
        }
    }


}
