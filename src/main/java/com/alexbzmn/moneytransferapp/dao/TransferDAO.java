package com.alexbzmn.moneytransferapp.dao;

import com.alexbzmn.moneytransferapp.model.transfer.Transfer;
import com.alexbzmn.moneytransferapp.model.transfer.TransferStatus;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@RegisterRowMapper(TransferDAO.TransferRowMapper.class)
public interface TransferDAO {

    @SqlUpdate("insert into transfer (from_account_id, to_account_id, amount, transfer_status, result_message) " +
            "values (:fromAccountId, :toAccountId, :amount, :transferStatus, :transferResultMessage)")
    @GetGeneratedKeys
    int insert(@BindBean Transfer transfer);

    @SqlQuery("select * from transfer")
    List<Transfer> fetchAll();

    @SqlQuery("select * from transfer where id = :id")
    Transfer findById(@Bind("id") int id);

    class TransferRowMapper implements RowMapper<Transfer> {
        @Override
        public Transfer map(ResultSet rs, StatementContext ctx) throws SQLException {
            return Transfer.builder()
                    .amount(rs.getBigDecimal("amount"))
                    .id(rs.getInt("id"))
                    .fromAccountId(rs.getInt("from_account_id"))
                    .toAccountId(rs.getInt("to_account_id"))
                    .transferStatus(TransferStatus.valueOf(rs.getString("transfer_status")))
                    .transferResultMessage(rs.getString("result_message"))
                    .build();
        }
    }
}
