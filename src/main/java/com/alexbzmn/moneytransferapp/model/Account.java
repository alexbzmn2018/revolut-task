package com.alexbzmn.moneytransferapp.model;

import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@Getter
public class Account {

    private final int id;

    @NotNull
    private BigDecimal balance;

    public void subtract(BigDecimal value) {
        balance = balance.subtract(value);
    }

    public void add(BigDecimal value) {
        balance = balance.add(value);
    }

    public boolean hasSufficientFunds(BigDecimal value) {
        return balance.compareTo(value) >= 0;
    }
}
