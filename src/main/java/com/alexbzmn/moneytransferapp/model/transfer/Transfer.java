package com.alexbzmn.moneytransferapp.model.transfer;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Builder
@Getter
public class Transfer {
    @Setter
    private Integer id;
    private final int fromAccountId;
    private final int toAccountId;
    private final String transferResultMessage;
    private final TransferStatus transferStatus;
    private final BigDecimal amount;
}
