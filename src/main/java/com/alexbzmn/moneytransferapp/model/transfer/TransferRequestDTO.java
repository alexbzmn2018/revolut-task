package com.alexbzmn.moneytransferapp.model.transfer;

import lombok.Builder;
import lombok.Getter;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@Getter
@Value
public class TransferRequestDTO {
    private final int fromAccountId;
    private final int toAccountId;

    @NotNull
    private final BigDecimal amount;

}
