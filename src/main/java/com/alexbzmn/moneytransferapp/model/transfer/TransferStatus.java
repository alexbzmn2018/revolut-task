package com.alexbzmn.moneytransferapp.model.transfer;

public enum TransferStatus {
    SUCCESS, FAILURE_NO_SENDER, FAILURE_NO_RECEIVER, FAILURE_INSUFFICIENT_FUNDS, FAILURE_INVALID_INPUT
}
