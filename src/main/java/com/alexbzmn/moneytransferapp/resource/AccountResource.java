package com.alexbzmn.moneytransferapp.resource;

import com.alexbzmn.moneytransferapp.dao.AccountDAO;
import com.alexbzmn.moneytransferapp.model.Account;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.List;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    private final AccountDAO accountDAO;

    public AccountResource(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @GET
    public List<Account> getAccounts() {
        return accountDAO.fetchAll();
    }

    @GET
    @Path("/{id}")
    public Account getById(@PathParam("id") int accountId) {
        return accountDAO.findById(accountId);
    }

    @POST
    public Response add(@NotNull @Valid Account account) {
        int accountId = accountDAO.insert(account);
        return Response.created(UriBuilder.fromResource(AccountResource.class)
                .path(String.valueOf(accountId)).build())
                .entity(accountId)
                .build();
    }
}
