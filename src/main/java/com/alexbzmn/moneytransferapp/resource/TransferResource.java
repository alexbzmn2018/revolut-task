package com.alexbzmn.moneytransferapp.resource;

import com.alexbzmn.moneytransferapp.model.transfer.Transfer;
import com.alexbzmn.moneytransferapp.model.transfer.TransferRequestDTO;
import com.alexbzmn.moneytransferapp.model.transfer.TransferStatus;
import com.alexbzmn.moneytransferapp.service.TransferService;
import lombok.extern.slf4j.Slf4j;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.List;

@Path("/transfer")
@Produces(MediaType.APPLICATION_JSON)
@Slf4j
public class TransferResource {

    private TransferService transferService;

    public TransferResource(TransferService transferService) {
        this.transferService = transferService;
    }

    @GET
    public List<Transfer> getTransfers() {
        return transferService.getAllTransfers();
    }

    @GET
    @Path("/{id}")
    public Transfer getById(@PathParam("id") int transferId) {
        return transferService.getById(transferId);
    }

    @POST
    public Response submitTransfer(@NotNull @Valid TransferRequestDTO transferRequestDTO) {
        Transfer transfer = transferService.submitTransfer(transferRequestDTO);
        if (transfer.getTransferStatus() == TransferStatus.SUCCESS) {
            return Response.created(UriBuilder
                    .fromResource(TransferResource.class)
                    .path(String.valueOf(transfer.getId())).build())
                    .entity(transfer)
                    .build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity(transfer).build();
        }
    }
}
