package com.alexbzmn.moneytransferapp.service;

import com.alexbzmn.moneytransferapp.dao.AccountDAO;
import com.alexbzmn.moneytransferapp.dao.TransferDAO;
import com.alexbzmn.moneytransferapp.model.Account;
import com.alexbzmn.moneytransferapp.model.transfer.Transfer;
import com.alexbzmn.moneytransferapp.model.transfer.TransferRequestDTO;
import com.alexbzmn.moneytransferapp.model.transfer.TransferStatus;
import org.jdbi.v3.core.Jdbi;

import java.math.BigDecimal;
import java.util.List;

public class TransferService {

    private final TransferDAO transferDAO;
    private final Jdbi jdbi;

    public TransferService(TransferDAO transferDAO, Jdbi jdbi) {
        this.transferDAO = transferDAO;
        this.jdbi = jdbi;
    }

    public Transfer submitTransfer(final TransferRequestDTO transferRequestDTO) {
        synchronized (jdbi) {
            return jdbi.inTransaction(handle -> {
                AccountDAO accountDAO = handle.attach(AccountDAO.class);

                Account fromAccount = accountDAO.findById(transferRequestDTO.getFromAccountId());
                Account toAccount = accountDAO.findById(transferRequestDTO.getToAccountId());

                return tryTransfer(transferRequestDTO, accountDAO, transferDAO, fromAccount, toAccount);
            });
        }
    }

    Transfer tryTransfer(TransferRequestDTO transferRequestDTO, AccountDAO accountDAO, TransferDAO transferDAO,
                         Account fromAccount, Account toAccount) {

        if (!isTransferRequestValid(transferRequestDTO)) {
            return createTransferDTO(transferRequestDTO, TransferStatus.FAILURE_INVALID_INPUT, "Invalid input");
        }

        if (fromAccount == null) {
            return createTransferDTO(transferRequestDTO, TransferStatus.FAILURE_NO_SENDER, "Sender account does not exist");
        }

        if (toAccount == null) {
            return createTransferDTO(transferRequestDTO, TransferStatus.FAILURE_NO_RECEIVER, "Receiver account does not exist");
        }

        if (!fromAccount.hasSufficientFunds(transferRequestDTO.getAmount())) {
            return createTransferDTO(transferRequestDTO, TransferStatus.FAILURE_INSUFFICIENT_FUNDS, "Sender has Insufficient Funds");
        }

        return performTransfer(transferRequestDTO, accountDAO, transferDAO, fromAccount, toAccount);
    }

    private Transfer performTransfer(TransferRequestDTO transferRequestDTO, AccountDAO accountDAO,
                                     TransferDAO transferDAO, Account fromAccount, Account toAccount) {
        fromAccount.subtract(transferRequestDTO.getAmount());
        toAccount.add(transferRequestDTO.getAmount());

        Transfer transfer = createTransferDTO(transferRequestDTO, TransferStatus.SUCCESS, "Transfer succeeded");
        int transferId = transferDAO.insert(transfer);
        transfer.setId(transferId);

        accountDAO.update(fromAccount);
        accountDAO.update(toAccount);

        return transfer;
    }

    private Transfer createTransferDTO(TransferRequestDTO transferRequestDTO, TransferStatus transferStatus, String message) {
        return Transfer.builder()
                .toAccountId(transferRequestDTO.getToAccountId())
                .fromAccountId(transferRequestDTO.getFromAccountId())
                .amount(transferRequestDTO.getAmount())
                .transferStatus(transferStatus)
                .transferResultMessage(message)
                .build();
    }

    private boolean isTransferRequestValid(TransferRequestDTO transferRequest) {
        if (transferRequest.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            return false;
        }
        return transferRequest.getFromAccountId() != transferRequest.getToAccountId();
    }

    public List<Transfer> getAllTransfers() {
        return transferDAO.fetchAll();
    }

    public Transfer getById(int id) {
        return transferDAO.findById(id);
    }
}
