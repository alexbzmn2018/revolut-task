package com.alexbzmn.moneytransferapp.resource;

import com.alexbzmn.moneytransferapp.dao.AccountDAO;
import com.alexbzmn.moneytransferapp.model.Account;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AccountResourceTest {

    private static AccountDAO accountDAO = Mockito.mock(AccountDAO.class);

    @ClassRule
    public static final ResourceTestRule resource = ResourceTestRule.builder()
            .addResource(new AccountResource(accountDAO)).build();

    @Test
    public void getAccountTest() {
        Account account = Account.builder().balance(new BigDecimal(1)).id(1).build();

        Mockito.when(accountDAO.fetchAll()).thenReturn(Collections.singletonList(account));
        Mockito.when(accountDAO.findById(1)).thenReturn(account);

        Account responseAccount = resource.target("/account/1").request().get().readEntity(Account.class);
        assertEquals(account.getId(), responseAccount.getId());
        assertEquals(account.getBalance(), responseAccount.getBalance());

        List<Account> accounts = resource.target("/account").request().get().readEntity(new GenericType<List<Account>>() {
        });
        assertEquals(1, accounts.size());
        assertEquals(account.getId(), accounts.get(0).getId());
    }

    @Test
    public void createAccountTest() {
        Account account = Account.builder().balance(new BigDecimal(1)).id(1).build();
        Mockito.when(accountDAO.insert(Matchers.any())).thenReturn(1);

        String path = "/account/1";
        Response response = resource.target("/account").request().post(Entity.entity(account, MediaType.APPLICATION_JSON_TYPE));

        assertEquals(path, response.getLocation().getPath());
        assertEquals(1, response.readEntity(Integer.class).intValue());

        response = resource.target("/account").request().post(Entity.entity(Account.builder().balance(null).id(1).build(), MediaType.APPLICATION_JSON_TYPE));
        assertEquals(422, response.getStatus());
    }

}