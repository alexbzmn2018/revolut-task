package com.alexbzmn.moneytransferapp.resource;

import com.alexbzmn.moneytransferapp.dao.TransferDAO;
import com.alexbzmn.moneytransferapp.model.transfer.Transfer;
import com.alexbzmn.moneytransferapp.model.transfer.TransferStatus;
import com.alexbzmn.moneytransferapp.service.TransferService;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.jdbi.v3.core.Jdbi;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import javax.ws.rs.core.GenericType;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TransferResourceTest {

    private static Jdbi jdbi = Mockito.mock(Jdbi.class);
    private static TransferDAO transferDAO = Mockito.mock(TransferDAO.class);

    @ClassRule
    public static final ResourceTestRule resource = ResourceTestRule.builder()
            .addResource(new TransferResource(new TransferService(transferDAO, jdbi)))
            .build();

    @Test
    public void getTransferTest() {
        Transfer transfer = Transfer.builder()
                .fromAccountId(1)
                .toAccountId(2)
                .amount(new BigDecimal(1))
                .transferStatus(TransferStatus.SUCCESS)
                .id(1)
                .build();

        Mockito.when(transferDAO.fetchAll()).thenReturn(Collections.singletonList(transfer));
        Mockito.when(transferDAO.findById(1)).thenReturn(transfer);

        Transfer responseTransfer = resource.target("/transfer/1").request().get().readEntity(Transfer.class);
        assertEquals(transfer.getId(), responseTransfer.getId());
        assertEquals(transfer.getTransferResultMessage(), responseTransfer.getTransferResultMessage());
        assertEquals(transfer.getTransferStatus(), responseTransfer.getTransferStatus());
        assertEquals(transfer.getAmount(), responseTransfer.getAmount());
        assertEquals(transfer.getFromAccountId(), responseTransfer.getFromAccountId());
        assertEquals(transfer.getToAccountId(), responseTransfer.getToAccountId());


        List<Transfer> transfers = resource.target("/transfer").request().get().readEntity(new GenericType<List<Transfer>>() {
        });
        assertEquals(1, transfers.size());
        assertEquals(transfer.getId(), transfers.get(0).getId());
    }

}
