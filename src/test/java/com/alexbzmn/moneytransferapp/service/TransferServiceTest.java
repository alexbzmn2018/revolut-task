package com.alexbzmn.moneytransferapp.service;

import com.alexbzmn.moneytransferapp.dao.AccountDAO;
import com.alexbzmn.moneytransferapp.dao.TransferDAO;
import com.alexbzmn.moneytransferapp.model.Account;
import com.alexbzmn.moneytransferapp.model.transfer.Transfer;
import com.alexbzmn.moneytransferapp.model.transfer.TransferRequestDTO;
import com.alexbzmn.moneytransferapp.model.transfer.TransferStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class TransferServiceTest {
    private TransferDAO transferDAO;
    private AccountDAO accountDAO;
    private TransferService transferService;

    @Before
    public void setup() {
        transferDAO = Mockito.mock(TransferDAO.class);
        accountDAO = Mockito.mock(AccountDAO.class);
        transferService = new TransferService(transferDAO, null);
    }

    @Test
    public void tryTransferSimpleTest() {
        Mockito.when(transferDAO.insert(Matchers.any())).thenReturn(1);

        TransferRequestDTO requestDTO = TransferRequestDTO.builder().amount(new BigDecimal(8.99)).fromAccountId(1).toAccountId(2).build();
        Account fromAccount = Account.builder().balance(new BigDecimal(10)).id(1).build();
        Account toAccount = Account.builder().balance(new BigDecimal(10)).id(2).build();

        Transfer transferResult = transferService.tryTransfer(requestDTO, accountDAO, transferDAO, fromAccount, toAccount);

        assertEquals(new BigDecimal(1.01).doubleValue(), fromAccount.getBalance().doubleValue(), 0.001);
        assertEquals(new BigDecimal(18.99).doubleValue(), toAccount.getBalance().doubleValue(), 0.001);
        assertEquals(new BigDecimal(8.99), transferResult.getAmount());
        assertEquals(1, transferResult.getFromAccountId());
        assertEquals(2, transferResult.getToAccountId());
        assertEquals(TransferStatus.SUCCESS, transferResult.getTransferStatus());
        assertEquals("Transfer succeeded", transferResult.getTransferResultMessage());
    }

    @Test
    public void tryTransferNullSenderTest() {
        TransferRequestDTO requestDTO = TransferRequestDTO.builder().amount(new BigDecimal(8.99)).fromAccountId(1).toAccountId(2).build();
        Account toAccount = Account.builder().balance(new BigDecimal(10)).id(2).build();

        Transfer transferResult = transferService.tryTransfer(requestDTO, accountDAO, transferDAO, null, toAccount);

        assertEquals(new BigDecimal(10).doubleValue(), toAccount.getBalance().doubleValue(), 0.001);
        assertEquals(new BigDecimal(8.99), transferResult.getAmount());
        assertEquals(2, transferResult.getToAccountId());
        assertEquals(TransferStatus.FAILURE_NO_SENDER, transferResult.getTransferStatus());
        assertEquals("Sender account does not exist", transferResult.getTransferResultMessage());
    }

    @Test
    public void tryTransferNullReceiverTest() {
        TransferRequestDTO requestDTO = TransferRequestDTO.builder().amount(new BigDecimal(8.99)).fromAccountId(1).toAccountId(2).build();
        Account fromAccount = Account.builder().balance(new BigDecimal(10)).id(1).build();

        Transfer transferResult = transferService.tryTransfer(requestDTO, accountDAO, transferDAO, fromAccount, null);

        assertEquals(new BigDecimal(10).doubleValue(), fromAccount.getBalance().doubleValue(), 0.001);
        assertEquals(new BigDecimal(8.99), transferResult.getAmount());
        assertEquals(2, transferResult.getToAccountId());
        assertEquals(TransferStatus.FAILURE_NO_RECEIVER, transferResult.getTransferStatus());
        assertEquals("Receiver account does not exist", transferResult.getTransferResultMessage());
    }

    @Test
    public void tryTransferNotEnoughMoneyTest() {
        Mockito.when(transferDAO.insert(Matchers.any())).thenReturn(1);

        TransferRequestDTO requestDTO = TransferRequestDTO.builder().amount(new BigDecimal(20)).fromAccountId(1).toAccountId(2).build();
        Account fromAccount = Account.builder().balance(new BigDecimal(10)).id(1).build();
        Account toAccount = Account.builder().balance(new BigDecimal(10)).id(2).build();

        Transfer transferResult = transferService.tryTransfer(requestDTO, accountDAO, transferDAO, fromAccount, toAccount);

        assertEquals(new BigDecimal(10).doubleValue(), fromAccount.getBalance().doubleValue(), 0.001);
        assertEquals(new BigDecimal(10).doubleValue(), toAccount.getBalance().doubleValue(), 0.001);
        assertEquals(new BigDecimal(20), transferResult.getAmount());
        assertEquals(1, transferResult.getFromAccountId());
        assertEquals(2, transferResult.getToAccountId());
        assertEquals(TransferStatus.FAILURE_INSUFFICIENT_FUNDS, transferResult.getTransferStatus());
        assertEquals("Sender has Insufficient Funds", transferResult.getTransferResultMessage());
    }

    @Test
    public void tryTransferNegativeAmountTest() {
        Mockito.when(transferDAO.insert(Matchers.any())).thenReturn(1);

        TransferRequestDTO requestDTO = TransferRequestDTO.builder().amount(new BigDecimal(-1)).fromAccountId(1).toAccountId(2).build();
        Account fromAccount = Account.builder().balance(new BigDecimal(10)).id(1).build();
        Account toAccount = Account.builder().balance(new BigDecimal(10)).id(2).build();

        Transfer transferResult = transferService.tryTransfer(requestDTO, accountDAO, transferDAO, fromAccount, toAccount);

        assertEquals(new BigDecimal(10).doubleValue(), fromAccount.getBalance().doubleValue(), 0.001);
        assertEquals(new BigDecimal(10).doubleValue(), toAccount.getBalance().doubleValue(), 0.001);
        assertEquals(new BigDecimal(-1), transferResult.getAmount());
        assertEquals(1, transferResult.getFromAccountId());
        assertEquals(2, transferResult.getToAccountId());
        assertEquals(TransferStatus.FAILURE_INVALID_INPUT, transferResult.getTransferStatus());
        assertEquals("Invalid input", transferResult.getTransferResultMessage());
    }

    @Test
    public void tryTransferSameAccountsTest() {
        Mockito.when(transferDAO.insert(Matchers.any())).thenReturn(1);

        TransferRequestDTO requestDTO = TransferRequestDTO.builder().amount(new BigDecimal(-1)).fromAccountId(1).toAccountId(1).build();
        Account fromAccount = Account.builder().balance(new BigDecimal(10)).id(1).build();

        Transfer transferResult = transferService.tryTransfer(requestDTO, accountDAO, transferDAO, fromAccount, fromAccount);

        assertEquals(new BigDecimal(10).doubleValue(), fromAccount.getBalance().doubleValue(), 0.001);
        assertEquals(new BigDecimal(-1), transferResult.getAmount());
        assertEquals(1, transferResult.getFromAccountId());
        assertEquals(1, transferResult.getToAccountId());
        assertEquals(TransferStatus.FAILURE_INVALID_INPUT, transferResult.getTransferStatus());
        assertEquals("Invalid input", transferResult.getTransferResultMessage());
    }


}