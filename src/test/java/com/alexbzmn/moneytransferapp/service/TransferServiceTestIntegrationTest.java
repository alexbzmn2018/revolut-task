package com.alexbzmn.moneytransferapp.service;


import com.alexbzmn.moneytransferapp.MoneyTransferApp;
import com.alexbzmn.moneytransferapp.MoneyTransferAppConfiguration;
import com.alexbzmn.moneytransferapp.model.Account;
import com.alexbzmn.moneytransferapp.model.transfer.Transfer;
import com.alexbzmn.moneytransferapp.model.transfer.TransferRequestDTO;
import com.alexbzmn.moneytransferapp.model.transfer.TransferStatus;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.db.ManagedDataSource;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import liquibase.Liquibase;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TransferServiceTestIntegrationTest {
    @ClassRule
    public static final DropwizardAppRule<MoneyTransferAppConfiguration> RULE =
            new DropwizardAppRule<>(MoneyTransferApp.class, ResourceHelpers.resourceFilePath("config-test.yml"));
    private static String accountEndpoint;
    private static String transferEndpoint;

    private static Client client;

    @BeforeClass
    public static void setup() throws Exception {
        ManagedDataSource ds = RULE.getConfiguration().getDatabase().build(
                RULE.getEnvironment().metrics(), "migrations");
        try (Connection connection = ds.getConnection()) {
            Liquibase migrator = new Liquibase("migrations.xml", new ClassLoaderResourceAccessor(), new JdbcConnection(connection));
            migrator.update("");
        }

        client = new JerseyClientBuilder(RULE.getEnvironment()).build("test client");
        accountEndpoint = String.format("http://localhost:%d/account", RULE.getLocalPort());
        transferEndpoint = String.format("http://localhost:%d/transfer", RULE.getLocalPort());
    }

    @Test
    public void transferMoneySimpleCaseTest() {
        Integer fromAccount = client.target(accountEndpoint)
                .request()
                .post(Entity.json(Account.builder()
                        .balance(new BigDecimal(100))
                        .build())).readEntity(Integer.class);
        Integer toAccount = client.target(accountEndpoint)
                .request()
                .post(Entity.json(Account.builder()
                        .balance(new BigDecimal(100))
                        .build())).readEntity(Integer.class);

        performTransfer(fromAccount, toAccount, 10);
        performTransfer(toAccount, fromAccount, 15);

        assertAccountBalanceEquals(fromAccount, 105);
        assertAccountBalanceEquals(toAccount, 95);

        int lastTransferId = performTransfer(fromAccount, toAccount, 5);

        assertAccountBalanceEquals(fromAccount, 100);
        assertAccountBalanceEquals(toAccount, 100);

        Transfer transfer = client.target(String.format(transferEndpoint + "/%d", lastTransferId))
                .request().get().readEntity(Transfer.class);

        assertEquals(lastTransferId, transfer.getId().intValue());
    }

    @Test
    public void transferMoneyConcurrentlyTest() throws InterruptedException {
        Integer fromAccount = client.target(accountEndpoint)
                .request()
                .post(Entity.json(Account.builder()
                        .balance(new BigDecimal(1000))
                        .build())).readEntity(Integer.class);
        Integer toAccount = client.target(accountEndpoint)
                .request()
                .post(Entity.json(Account.builder()
                        .balance(new BigDecimal(1000))
                        .build())).readEntity(Integer.class);

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        int nTransfers = 1000;
        for (int i = 1; i <= nTransfers; i++) {
            executorService.submit(() -> performTransfer(fromAccount, toAccount, 1));
            executorService.submit(() -> performTransfer(toAccount, fromAccount, 1));
        }

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);

        assertAccountBalanceEquals(fromAccount, 1000);
        assertAccountBalanceEquals(toAccount, 1000);

        Response response = client.target(transferEndpoint)
                .request()
                .get();

        List<Transfer> transfers = response.readEntity(new GenericType<List<Transfer>>() {
        });

        assertTrue(transfers.size() >= nTransfers);
    }

    @Test
    public void transferMoneyFloatingPointCaseTest() {
        Integer fromAccount = client.target(accountEndpoint)
                .request()
                .post(Entity.json(Account.builder()
                        .balance(new BigDecimal(0))
                        .build())).readEntity(Integer.class);
        Integer toAccount = client.target(accountEndpoint)
                .request()
                .post(Entity.json(Account.builder()
                        .balance(new BigDecimal(200))
                        .build())).readEntity(Integer.class);

        performTransfer(toAccount, fromAccount, 7.78986);

        assertAccountBalanceEquals(fromAccount, 7.79);
        assertAccountBalanceEquals(toAccount, 192.21);
    }

    @Test
    public void transferMoneyNoSenderTest() {
        Integer toAccount = client.target(accountEndpoint)
                .request()
                .post(Entity.json(Account.builder()
                        .balance(new BigDecimal(200))
                        .build())).readEntity(Integer.class);

        Response response = client.target(transferEndpoint)
                .request()
                .post(Entity.json(buildTransferRequest(-1, toAccount, new BigDecimal(100))));
        Transfer transfer = response
                .readEntity(Transfer.class);

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals(TransferStatus.FAILURE_NO_SENDER, transfer.getTransferStatus());
    }

    @Test
    public void transferMoneyNoReceiverTest() {
        Integer fromAccount = client.target(accountEndpoint)
                .request()
                .post(Entity.json(Account.builder()
                        .balance(new BigDecimal(200))
                        .build())).readEntity(Integer.class);

        Response response = client.target(transferEndpoint)
                .request()
                .post(Entity.json(buildTransferRequest(fromAccount, -1, new BigDecimal(100))));
        Transfer transfer = response
                .readEntity(Transfer.class);

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals(TransferStatus.FAILURE_NO_RECEIVER, transfer.getTransferStatus());
    }

    @Test
    public void transferMoneyInsufficientFundsTest() {
        Integer fromAccount = client.target(accountEndpoint)
                .request()
                .post(Entity.json(Account.builder()
                        .balance(new BigDecimal(200))
                        .build())).readEntity(Integer.class);

        Integer toAccount = client.target(accountEndpoint)
                .request()
                .post(Entity.json(Account.builder()
                        .balance(new BigDecimal(200))
                        .build())).readEntity(Integer.class);

        Response response = client.target(transferEndpoint)
                .request()
                .post(Entity.json(buildTransferRequest(fromAccount, toAccount, new BigDecimal(300))));
        Transfer transfer = response
                .readEntity(Transfer.class);

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals(TransferStatus.FAILURE_INSUFFICIENT_FUNDS, transfer.getTransferStatus());
    }

    @Test
    public void transferMoneyInvalidTransferRequestTest() {
        Integer fromAccount = client.target(accountEndpoint)
                .request()
                .post(Entity.json(Account.builder()
                        .balance(new BigDecimal(200))
                        .build())).readEntity(Integer.class);

        Response response = client.target(transferEndpoint)
                .request()
                .post(Entity.json(buildTransferRequest(fromAccount, fromAccount, new BigDecimal(300))));
        Transfer transfer = response.readEntity(Transfer.class);

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals(TransferStatus.FAILURE_INVALID_INPUT, transfer.getTransferStatus());
        assertEquals("Invalid input", transfer.getTransferResultMessage());

        response = client.target(transferEndpoint)
                .request()
                .post(Entity.json(buildTransferRequest(fromAccount, fromAccount, new BigDecimal(-1d))));
        transfer = response.readEntity(Transfer.class);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals(TransferStatus.FAILURE_INVALID_INPUT, transfer.getTransferStatus());
        assertEquals("Invalid input", transfer.getTransferResultMessage());

        response = client.target(transferEndpoint)
                .request()
                .post(Entity.json(buildTransferRequest(fromAccount, fromAccount, null)));
        assertEquals(422, response.getStatus());

    }

    private void assertAccountBalanceEquals(int accountId, double balance) {
        Account account = client.target(String.format(accountEndpoint + "/%d", accountId))
                .request()
                .get()
                .readEntity(Account.class);

        assertEquals(balance, account.getBalance().doubleValue(), 0);
    }

    private int performTransfer(int from, int to, double amount) {
        Response response = client.target(transferEndpoint)
                .request()
                .post(Entity.json(buildTransferRequest(from, to, new BigDecimal(amount))));
        Transfer transfer = response
                .readEntity(Transfer.class);

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        assertEquals(TransferStatus.SUCCESS, transfer.getTransferStatus());
        Assert.assertNotNull(transfer.getTransferResultMessage());

        return transfer.getId();
    }

    private TransferRequestDTO buildTransferRequest(int from, int to, BigDecimal amount) {
        return TransferRequestDTO.builder()
                .amount(amount)
                .fromAccountId(from)
                .toAccountId(to)
                .build();
    }

}
